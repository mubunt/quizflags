#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel Rizzo.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public LicenseS
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: quizFlags
# QUIZ: All Country Flags of the World
#-------------------------------------------------------------------------------
COPYRIGHT   = "2020"
DIRS        = src flags icons
SHORTCUT 	= quizFlags.desktop
ifneq ("$(wildcard $$HOME/Desktop)", "")
	DESKTOP 	= $$HOME/Desktop
else
	DESKTOP 	= $$HOME/Bureau
endif

include .make/topLevel.mk

release::
	@echo "------------------------------------------------------------"
	@echo "-- Generation of shortcut file $(SHORTCUT) in $(DESKTOP)"
	@rm -f $(DESKTOP)/$(SHORTCUT)
	@echo "[Desktop Entry]" > $(DESKTOP)/$(SHORTCUT)
	@echo "Version=1.0" >> $(DESKTOP)/$(SHORTCUT)
	@echo "Type=Application" >> $(DESKTOP)/$(SHORTCUT)
	@echo "Name=QUIZ FLAGS" >> $(DESKTOP)/$(SHORTCUT)
	@echo "Comment=" >> $(DESKTOP)/$(SHORTCUT)
	@echo "Exec=xfce4-terminal --geometry=80x60+1400+60 --color-bg=#4B0082 --color-text=#FFFFFF --command='$(BIN_DIR)/quizFlags' --title='QUIZ - ALL COUNTRY FLAGS OF THE WORLD'" >> $(DESKTOP)/$(SHORTCUT)
	@echo "Icon=$(BIN_DIR)/quizFlags_bin/quizFlags.png" >> $(DESKTOP)/$(SHORTCUT)
	@echo "Path=$(BIN_DIR)" >> $(DESKTOP)/$(SHORTCUT)
	@echo "Terminal=false" >> $(DESKTOP)/$(SHORTCUT)
	@echo "StartupNotify=false" >> $(DESKTOP)/$(SHORTCUT)
	@echo "GenericName=QUIZ FLAGS" >> $(DESKTOP)/$(SHORTCUT)
	@echo "Name[fr_FR]=QUIZFLAGS" >> $(DESKTOP)/$(SHORTCUT)
	@chmod +x $(DESKTOP)/$(SHORTCUT)
#-------------------------------------------------------------------------------

