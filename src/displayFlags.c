//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: quizFlags
// QUIZ: All Country Flags of the World
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdarg.h>
#include <libgen.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "displayFlags.h"
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static s_flag_description flag;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _flag_freeResources( void *buf ) {
	if ( buf != NULL )
		free( buf );
	if ( flag.ximage != NULL )
		XDestroyImage( flag.ximage );
	if ( flag.display != 0) {
		if ( flag.window != 0 ) {
			XUnmapWindow( flag.display, flag.window );
			XDestroyWindow( flag.display, flag.window );
		}
		XCloseDisplay( flag.display );
	}
	flag.ximage = NULL;
	flag.display = NULL;
	flag.data = NULL;
	flag.gc = NULL;
	flag.window = 0;
	flag.screen = 0;
	flag.width = flag.height = 0;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static Window _flag_createWindow( Display *display, int screen, int x, int y, unsigned int width, unsigned int height ) {
	XSetWindowAttributes attributes;
	attributes.background_pixel = WhitePixel( display, screen );
//	attributes.background_pixel = BlackPixel( display, screen );
	attributes.event_mask = ExposureMask;
	Window window = XCreateWindow(	/* Display*/ 	display,
	                /* Parent */					RootWindow(display, screen),
	                /* X */							x,
	                /* Y */							y,
	                /* Width */						width,
	                /* Heigth */					height,
	                /* Border Width */				0,
	                /* Depth */						CopyFromParent, //DefaultDepth(display, screen),
	                /* Class */						InputOutput,
	                /* Visual */					DefaultVisual(display, screen),
	                /* Value Mask */				CWBackPixel | CWEventMask,
	                /* Attributes */				&attributes );
	if ( ! window ) return window;
	Atom window_type = XInternAtom(	/* Display */	display,
	                   /* Atom name */				"_NET_WM_WINDOW_TYPE",
	                   /* only_if_exist */			false );
	long unsigned value = XInternAtom(	/* Display */	display,
	                      /* Atom name */				"_NET_WM_WINDOW_TYPE_DOCK",
	                      /* only_if_exist */			false );
	XChangeProperty(	/* Display */		display,
	                                        /* Window */					window,
	                                        /* Atom Property */				window_type,
	                                        /* Property type */				XA_ATOM,
	                                        /* Format */					32,
	                                        /* Operation mode */			PropModeReplace,
	                                        /* Property data */				(unsigned char *) &value,
	                                        /* Nb property data elements */	1 );
	return window;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static XImage *_flag_createImage( Display *display, Visual *visual, char *data, unsigned int width, unsigned int height, int rowbytes ) {
	return XCreateImage(	/* Display */	display,
	                                        /* Visual */					visual,
	                                        /* Depth */						24,
	                                        /* Image Format */				ZPixmap,
	                                        /* Offset */					0,
	                                        /* Data */						data,
	                                        /* Image Width */				width,
	                                        /* Image Height */				height,
	                                        /* Bitmap_pad */				8,
	                                        /* Bytes_per_line */			rowbytes );
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _flag_putImage( Display *display, Window window, GC gc, XImage *ximage, unsigned int width, unsigned int height ) {
	return XPutImage(	/* Display */							display,
	        /* Window */										window,
	        /* Graphics context */								gc,
	        /* XImage */										ximage,
	        /* Offset in X from the left edge of the image */	0,
	        /* Offset in Y from the top edge of the image */	0,
	        /* Abscissa of the image in window */				0,
	        /* Ordinate of the image in window */				0,
	        /* Image width */									width,
	        /* Image height */									height );
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// From https://stackoverflow.com/questions/17857575/displaying-png-file-using-xputimage-does-not-work
static int _flag_loadPngFile( char *filePNG, unsigned char **data, unsigned int *rowbytes, unsigned int *width, unsigned int *height ) {
	unsigned char header[8];    // 8 is the maximum size that can be checked
	// Open PNG file
	FILE *fptr = fopen(filePNG, "rb");
	if ( fptr == NULL ) return FLAG_FAILURE;
	size_t unused = fread(header, 1, 8, fptr);
	if ( png_sig_cmp(header, 0, 8)) {
		fclose( fptr );
		return FLAG_FAILURE;
	}
	//  Allocate and initialize a png_struct structure for reading PNG file
	png_structp png = png_create_read_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
	if ( ! png ) {
		fclose( fptr );
		return FLAG_FAILURE;
	}
	// Allocate and initialize a png_info structure
	png_infop info = png_create_info_struct ( png );
	if ( ! info ) {
		png_destroy_read_struct( &png, (png_infopp) NULL, (png_infopp) NULL);
		fclose( fptr );
		return FLAG_FAILURE;
	}
	// Initialize the default input/output functions for the PNG file to standard C streams
	png_init_io( png, fptr );
	// Just to inform that we already read some bytes to test the nature of the file
	png_set_sig_bytes(png, 8);
	// Read the PNG image information
	png_read_info( png, info );
	// Get information from png_info structure
	*width =  png_get_image_width(png, info);
	*height = png_get_image_height(png, info);
	int colortype = png_get_color_type(png, info);
	// Get number of bytes needed to hold a row
	*rowbytes = (unsigned int) png_get_rowbytes( png, info );
	if (colortype == PNG_COLOR_TYPE_RGB) {
		// X hates 24bit images - pad to flagRGBA
		png_set_filler( png, 0xff, PNG_FILLER_AFTER );
		*rowbytes = (*rowbytes * 4) / 3;
	}
	// PNG files store 3 color pixels in red, green, blue order. This code would be used if they are supplied as blue, green, red
	png_set_bgr( png );
	// Size of the image to load
	size_t size = *height * *rowbytes;
	// Allocate memory for image. This will be freed by XDestroyImage
	*data = malloc( size * sizeof (png_byte) );
	if ( *data == NULL ) {
		png_destroy_read_struct( &png, &info, (png_infopp) NULL);
		fclose( fptr );
		return FLAG_FAILURE;
	}
	// Allocate array of pointers to image rows
	unsigned char **rowPointers = malloc( *height * sizeof(unsigned char *) );
	if (rowPointers == NULL ) {
		png_destroy_read_struct( &png, &info, (png_infopp) NULL);
		free( *data );
		fclose( fptr );
		return FLAG_FAILURE;
	}
	// Initialization of array of pointers to image rows
	png_bytep ptdata = *data;
	for (unsigned int i = 0; i < *height; ++i, ptdata += *rowbytes)
		rowPointers[i] = ptdata;
	// Read the entire image into memory
	png_read_image(png, rowPointers);
	// Finishing a sequential read and close the file
	png_read_end(png, NULL);
	fclose( fptr );
	// free the memory associated with read png_struct
	png_destroy_read_struct( &png, &info, (png_infopp) NULL);
	free (rowPointers);
	return FLAG_SUCCESS;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
int flag_create( char *fileimage ) {
	//--- Initializations
	memset( &flag, 0, sizeof(flag));
	//--- Allocate memory for image and read the image from file
	unsigned int rowbytes = 0;
	unsigned int width_ = 0;
	unsigned int height_ = 0;
	int n = _flag_loadPngFile( fileimage, &flag.data, &rowbytes, &width_, &height_ );
	if ( n != FLAG_SUCCESS ) return n;
	flag.width = width_;
	flag.height = height_;
	//--- Open a connection with the X server
	flag.display = XOpenDisplay( NULL );
	if ( ! flag.display ) return FLAG_FAILURE;
	//--- Get the default screen and its size
	flag.screen = DefaultScreen( flag.display );
	Screen *pScreen = ScreenOfDisplay( flag.display, flag.screen );
	//--- Compute flag screen coordinates
	flag.x = ( pScreen->width - (int) flag.width ) / 2;
	flag.y = ( pScreen->height - (int) flag.height ) / 2;
	//--- Create the flag window
	flag.window = _flag_createWindow( flag.display, flag.screen, flag.x, flag.y, flag.width, flag.height );
	if ( ! flag.window ) return FLAG_FAILURE;
	//--- Map and show flag window
	XSelectInput( flag.display, flag.window, ExposureMask | KeyPressMask);
	XMapWindow( flag.display, flag.window );
	XFlush( flag.display );
	//--- Allocate a new GC (graphics context) for drawing in the window.
	flag.gc = XCreateGC(	/* Display*/ 		flag.display,
	          /* Window */		flag.window,
	          /* Value Mask */	0,
	          /* Attributes */	NULL );
	if ( ! flag.gc ) return FLAG_FAILURE;
	//--- Create the XImage
	flag.ximage = _flag_createImage( flag.display, DefaultVisualOfScreen( pScreen ), (char *) flag.data, flag.width, flag.height, (int) rowbytes );
	if ( flag.ximage == NULL )  {
		_flag_freeResources( flag.data );
		return FLAG_FAILURE;
	}
	//--- Draw the XImage to the window
	if (0 != _flag_putImage( flag.display, flag.window, flag.gc, flag.ximage, flag.width, flag.height )) {
		_flag_freeResources( NULL );
		return FLAG_FAILURE;
	}
	//--- Flush output buffer
	XFlush(	flag.display );

	XEvent event;
	bool exit = false;
	while ( ! exit ) {
		XNextEvent( flag.display, &event);
		if (event.type == Expose)
			_flag_putImage( flag.display, flag.window, flag.gc, flag.ximage, flag.width, flag.height );
		exit = true;
	}
	return FLAG_SUCCESS;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int flag_destroy( void ) {
	_flag_freeResources( NULL );
	return FLAG_SUCCESS;
}
//------------------------------------------------------------------------------
