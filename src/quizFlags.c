//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: quizFlags
// QUIZ: All Country Flags of the World
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <libgen.h>
#include <stdbool.h>
#include <errno.h>
#include <termios.h>
#include <getopt.h>
#include <wchar.h>
#include <locale.h>
#include <sys/stat.h>

#include <sodium.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "displayFlags.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define QUIZ 						"quizFlags"
#define COPYRIGHT					"Copyright (c) 2020, Michel RIZZO. All Rights Reserved."
#define PURPOSE						"QUIZ: All Country Flags of the World"

#ifdef VERSION
#define QUIZ_VERSION(x) 			str(x)
#define str(x)						#x
#else
#define QUIZ_VERSION 				"Unknown"
#endif

#define PROCESSPSEUDOFILESYSTEM		"/proc/self/exe"
#define FLAGS_DIRECTORY				"/flags"
#define DEVELOP_DIRECTORY			"linux"
#define BIN_DIRECTORY				"/quizFlags_bin"

#define EXIST(x)					(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)					(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
#define DRAWLINE()					fprintf(stdout, "━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n")
#define MAX_COUNTRY_SIZE			45
#define MAX_PROPOSALS				10
#define MAX_QUESTIONS				10

#define ATTRIBUTS_TITLE				"\033[1;48;5;12;38;5;15m"
#define ATTRIBUTS_SCORE				"\033[1;48;5;15;38;5;12m"
#define ATTRIBUTS_ERROR				"\033[1;38;5;1m"
#define ATTRIBUTS_GOOD				"\033[1;38;5;2m"
#define ATTRIBUTS_BAD				"\033[1;38;5;9m"
#define ATTRIBUTS_OFF				"\033[0m"

#define BS 							0x7f
#define CR 							0x0d
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct s_country_flag {
	const char *country;
	const char *flag;
} s_country_flag;
//------------------------------------------------------------------------------
// ENUM
//------------------------------------------------------------------------------
typedef enum { MODE_ECHOED, MODE_RAW } terminal;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int							number_of_proposals = 10;
static int							number_of_questions	= 5;
static int							number_of_countries	= 0;
static int 							number_of_correct_answers = 0;

#include "quizFlags.h"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void version(char *exec) {
	fprintf(stdout, "%s - %s\n%s - Version %s\n\n", QUIZ, COPYRIGHT, basename(exec), QUIZ_VERSION(VERSION));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void usage(char *exec) {
	version(exec);
	fprintf(stdout, "%s\n\nUsage: %s [OPTIONS]\n\n", PURPOSE, basename(exec));
	fprintf(stdout, "  -h, --help            Print this help and exit\n");
	fprintf(stdout, "  -V, --version         Print version and exit\n");
	fprintf(stdout, "\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _error( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "%sERROR: %s. Abandon!%s\n\n", ATTRIBUTS_ERROR, buff, ATTRIBUTS_OFF);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void _set_Terminal_Mode( terminal mode ) {
	static struct termios cooked, raw;
	switch (mode) {
	case MODE_ECHOED:
		tcsetattr(STDIN_FILENO, TCSANOW, &cooked);
		break;
	case MODE_RAW:
		tcgetattr(STDIN_FILENO, &cooked);
		raw = cooked;
		cfmakeraw(&raw);
		tcsetattr(STDIN_FILENO, TCSANOW, &raw);
		break;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _read_an_integer( void ) {
	char answ[10];
	_set_Terminal_Mode(MODE_RAW);
	bool done = false;
	char *pt = answ;
	do {
		int c = getchar();
		switch (c) {
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case '0':
			if ((long unsigned int) (pt - answ) < sizeof(answ)) {
				fprintf(stdout, "%c", c);
				fflush(stdout);
				*pt = (char) c;
				pt++;
			}
			break;
		case BS:
			if (pt > answ) {
				fprintf(stdout, "\b \b");
				fflush(stdout);
				pt--;
			}
			break;
		case CR:
			fprintf(stdout, "%c", c);
			fflush(stdout);
			done = true;
			break;
		default:
			break;
		}
	} while ( ! done );
	*pt = '\0';
	_set_Terminal_Mode(MODE_ECHOED);
	fprintf(stdout, "\n");
	return (int) strtol(answ, (char **)NULL, 10);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool _read_again( void ) {
	bool again = false;
	fprintf(stdout, "\n");
	fprintf(stdout, "Ré-essayons-nous [ O | n ] ? ");
	fflush(stdout);
	_set_Terminal_Mode(MODE_RAW);
	int c = getchar();
	fprintf(stdout, "%c", c);
	if (c == 'o' || c == 'O') again = true;
	_set_Terminal_Mode(MODE_ECHOED);
	fprintf(stdout, "\n");
	return again;;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _print_Header( void ) {
	fprintf(stdout, "\n");
	fprintf(stdout, "    %s ╔═════════════════════════════════════════╗ %s\n", ATTRIBUTS_TITLE, ATTRIBUTS_OFF);
	fprintf(stdout, "    %s ║  QUIZ - ALL COUNTRY FLAGS OF THE WORLD  ║ %s\n", ATTRIBUTS_TITLE, ATTRIBUTS_OFF);
	fprintf(stdout, "    %s ╚═════════════════════════════════════════╝ %s\n", ATTRIBUTS_TITLE, ATTRIBUTS_OFF);
	fprintf(stdout, "\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _print_Context( int countries, int questions, int proposals ) {
	fprintf(stdout, "\n");
	fprintf(stdout, "%s Nombre de pays ........................ %3d %s\n", ATTRIBUTS_SCORE, countries, ATTRIBUTS_OFF);
	fprintf(stdout, "%s Nombre de questions du quiz ........... %3d %s\n", ATTRIBUTS_SCORE, questions, ATTRIBUTS_OFF);
	fprintf(stdout, "%s Nombre de propositions par question ... %3d %s\n", ATTRIBUTS_SCORE, proposals, ATTRIBUTS_OFF);
	fprintf(stdout, "\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _print_Question( int n, int nbproposals, char *countries ) {
	DRAWLINE();
	fprintf(stdout, "Question n°%d - Le drapeau affiché est celui de : \n", n);
	for (int j = 0; j < nbproposals; j++) {
		fprintf(stdout, "\t%2d - %s\n", j + 1, countries + (long unsigned int) j * MAX_COUNTRY_SIZE);
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _print_Result( bool res, const char *answer, char *useranswer ) {
	if (res)
		fprintf(stdout, "\t%sBONNE réponse. C'est bien le drapeau de %s.%s\n\n", ATTRIBUTS_GOOD, answer, ATTRIBUTS_OFF);
	else
		fprintf(stdout, "\t%sMAUVAISE réponse. C'est le drapeau de %s et non pas de %s.%s\n\n", ATTRIBUTS_BAD, answer, useranswer, ATTRIBUTS_OFF);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _print_Score( int correct, int questions) {
	DRAWLINE();
	fprintf(stdout, "%s                                                   %s\n", ATTRIBUTS_SCORE, ATTRIBUTS_OFF);
	fprintf(stdout, "%s Score: %2d réponse(s) bonne(s) sur %2d question(s). %s\n", ATTRIBUTS_SCORE, correct, questions, ATTRIBUTS_OFF);
	fprintf(stdout, "%s                                                   %s\n", ATTRIBUTS_SCORE, ATTRIBUTS_OFF);
	fprintf(stdout, "\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _random_Draw( int max ) {
	return (int) randombytes_uniform((uint32_t)max);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _compare(void const *a, void const *b) {
	return strcmp(a, b);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static wchar_t *_cleaning_string( const char *str ) {
	size_t mbslen = mbstowcs(NULL, str, 0);
	wchar_t *wstr = malloc((mbslen + 1) * sizeof(wchar_t));
	(void) mbstowcs(wstr, str, mbslen + 1);
	wchar_t *pt = wstr;
	while (*pt != '\0') {
		*pt = (unsigned char) tolower(*pt);
		switch (*pt) {
		case 224:
			*pt = 'a';
			break;
		case 225:
			*pt = 'a';
			break;
		case 226:
			*pt = 'a';
			break;
		case 227:
			*pt = 'a';
			break;
		case 228:
			*pt = 'a';
			break;
		case 229:
			*pt = 'a';
			break;
		case 231:
			*pt = 'c';
			break;
		case 232:
			*pt = 'e';
			break;
		case 233:
			*pt = 'e';
			break;
		case 234:
			*pt = 'e';
			break;
		case 235:
			*pt = 'e';
			break;
		case 236:
			*pt = 'i';
			break;
		case 237:
			*pt = 'i';
			break;
		case 238:
			*pt = 'i';
			break;
		case 239:
			*pt = 'i';
			break;
		case 240:
			*pt = 'o';
			break;
		case 241:
			*pt = 'n';
			break;
		case 242:
			*pt = 'o';
			break;
		case 243:
			*pt = 'o';
			break;
		case 244:
			*pt = 'o';
			break;
		case 245:
			*pt = 'o';
			break;
		case 246:
			*pt = 'o';
			break;
		case 249:
			*pt = 'u';
			break;
		case 250:
			*pt = 'u';
			break;
		case 251:
			*pt = 'u';
			break;
		case 252:
			*pt = 'u';
			break;
		case 253:
			*pt = 'y';
			break;
		case 255:
			*pt = 'y';
			break;
		case L'-':
			*pt = ' ';
			break;
		default:
			break;
		}
		pt++;
	}
	return wstr;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _read_Questions( void ) {
	int n;
	while (1) {
		fprintf(stdout, "Combien de questions dans ce quiz (0..%d) ? ", MAX_QUESTIONS);
		fflush(stdout);
		n = _read_an_integer();
		if (n <= MAX_QUESTIONS) break;
		fprintf(stdout, "\n\tRéponse incorrecte.\r\n");
	}
	return n;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _read_Proposals( void ) {
	int n;
	while (1) {
		fprintf(stdout, "Combien de propositions par question (0, 2..%d) ? ", MAX_PROPOSALS);
		fflush(stdout);
		n = _read_an_integer();
		if (n == 0 || (n >= 2 && n <= MAX_QUESTIONS)) break;
		fprintf(stdout, "\n\tRéponse incorrecte.\r\n");
	}
	return n;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _read_Answer( int nbcountries ) {
	fprintf(stdout, "\n");
	int n;
	while (1) {
		fprintf(stdout, "\tRéponse : ");
		fflush(stdout);
		n = _read_an_integer();
		if (n > 0 && n <= nbcountries) break;
		fprintf(stdout, "\n\tRéponse incorrecte.\r\n");
		fflush(stdout);
	}
	return n;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int _read_Country( int n, char *answ ) {
	DRAWLINE();
	fprintf(stdout, "Question n°%d - De quel pays est le drapeau affiché : ", n);
	fflush(stdout);
	_set_Terminal_Mode(MODE_RAW);
	char *pt = answ;
	int c = getchar();
	while (c != CR) {
		if (c != BS) {
			fprintf(stdout, "%c", c);
			fflush(stdout);
			*pt = (char) c;
			pt++;
		} else {
			if (pt > answ) {
				fprintf(stdout, "\b \b");
				fflush(stdout);
				pt--;
			}
		}
		c = getchar();
	}
	*pt = '\0';
	_set_Terminal_Mode(MODE_ECHOED);
	if (strlen(answ) == 0) strcat(answ, "<vide>");
	fprintf(stdout, "\n");
	return (int) strlen(answ);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct stat locstat, *ptlocstat;        // stat variables for EXIST*
	char *ptarray_of_countries = NULL;
	int *ptarray_of_idxcountries = NULL;
	//---- Initialization ------------------------------------------------------
	setlocale(LC_ALL, "");
	ptlocstat = &locstat;
	number_of_countries = sizeof(country_flag) / sizeof(country_flag[0]);
	if (sodium_init() < 0) {
		_error("%s", "Le programme de tirage aléatoire ne peut être initialisé");
		return EXIT_FAILURE;
	}
	ptarray_of_idxcountries = calloc((size_t) number_of_proposals, sizeof(int));
	if (ptarray_of_idxcountries == NULL) {
		_error("%s", "Allocation mémoire pour indice pays tirés aléatoirement impossible");
		goto EXIT;
	}
	ptarray_of_countries = calloc((size_t) number_of_proposals, MAX_COUNTRY_SIZE);
	if (ptarray_of_countries == NULL) {
		_error("%s", "Allocation mémoire pour pays tirés aléatoirement impossible");
		goto EXIT;
	}
	//---- Flags path determination ---------------------------------------------
	char flagspath[PATH_MAX];
	if (readlink(PROCESSPSEUDOFILESYSTEM, flagspath, sizeof(flagspath)) == -1) {
		_error("%s", "Acquisition du répertoire du programme impossible");
		goto EXIT;
	}
	dirname(flagspath);
	char execdir[PATH_MAX];
	strcpy(execdir, flagspath);
	strcpy(execdir, basename(execdir));
	if (strcmp(execdir, DEVELOP_DIRECTORY) == 0)
		dirname(flagspath);
	else
		strcat(flagspath, BIN_DIRECTORY);
	strcat(flagspath, FLAGS_DIRECTORY);
	if (! EXISTDIR(flagspath)) {
		_error("MAUVAISE INSTALLATION - Le répertoire '%s' n'existe pas", flagspath);
		goto EXIT;
	}
	//---- Option parsing ------------------------------------------------------
	struct option long_options[] = {
		{ "help",			no_argument,		0,	'h' },
		{ "version",		no_argument,		0,	'V' },
		{ 0,				0,					0,	0 }
	};
	int option_index = 0;	// getopt_long stores the option index here.
	int opt;
	char *configfile = NULL;
	opterr = 0;
	while ((opt = getopt_long(argc, argv, "hVc:", long_options, &option_index)) != -1) {
		switch (opt) {
		case 0:				// If this option set a flag, do nothing else now.
			break;
		case 'h':			// Help
			usage(argv[0]);
			goto END;
		case 'V':			// Version
			version(argv[0]);
			goto END;
		case '?':			// getopt_long already printed an error message.
		default:
			goto EXIT;
		}
	}
	//---- Header printing ----------------------------------------------------
	_print_Header();
	number_of_questions = _read_Questions();
	if (number_of_questions == 0) goto END;
	number_of_proposals = _read_Proposals();
	if (number_of_proposals == 0) number_of_proposals++;
	_print_Context( number_of_countries, number_of_questions, number_of_proposals );
	//---- Go on ---------------------------------------------------------------
	bool again = false;
	do {
		number_of_correct_answers = 0;
		for ( int round = 0; round < number_of_questions; round++ ) {
			//---- Drawing 'number_of_proposals' different countries/flags
			int i;
			for (i = 0; i < number_of_proposals; i++) ptarray_of_idxcountries[i] = -1;
			i = 0;
			do {
				int n = _random_Draw(number_of_countries);	// n = 0 ... number_of_countries - 1
				bool found = false;
				for (int j = 0; j < i && ! found; j++) {
					if (n == ptarray_of_idxcountries[j]) found = true;
				}
				if ( ! found) {
					ptarray_of_idxcountries[i] = n;
					strcpy(ptarray_of_countries + (long unsigned int) i * MAX_COUNTRY_SIZE, country_flag[n].country);
					//fprintf(stdout, "==> %d %s\n", ptarray_of_idxcountries[i], ptarray_of_countries + (long unsigned int) i * MAX_COUNTRY_SIZE);
					i++;
				}
			} while (i != number_of_proposals);
			//---- Alphabetically sort drawed countries
			qsort(ptarray_of_countries, (size_t) number_of_proposals, MAX_COUNTRY_SIZE, _compare);
			//---- Testing if flag file exist!!!
			char flagfile[PATH_MAX * 2];
			sprintf(flagfile, "%s/%s", flagspath, country_flag[ptarray_of_idxcountries[0]].flag);
			if (! EXIST(flagfile)) {
				_error("MAUVAISE INSTALLATION - Fichier '%s' n'existe pas", flagfile);
				goto EXIT;
			}
			//---- Display falg file
			if (FLAG_SUCCESS != flag_create(flagfile)) {
				_error("Problème interne avec affichage");
				goto EXIT;
			}
			if (number_of_proposals == 1) {
				//---- Printing question and reading answer
				char answer[3 * MAX_COUNTRY_SIZE];
				wchar_t wanswer[3 * MAX_COUNTRY_SIZE], right_answer[3 * MAX_COUNTRY_SIZE];
				int n = _read_Country(round + 1, answer);
				wchar_t *pt1 = _cleaning_string(country_flag[ptarray_of_idxcountries[0]].country);
				wchar_t *pt2 = _cleaning_string(answer);
				bool z = (wcscmp(pt1, pt2) == 0);
				_print_Result(z ? true : false, country_flag[ptarray_of_idxcountries[0]].country, answer);
				if (z) number_of_correct_answers++;
				free(pt1);
				free(pt2);
			} else {
				//---- Printing question
				_print_Question(round + 1, number_of_proposals, ptarray_of_countries);
				//---- Read answer
				int n = _read_Answer(number_of_proposals);
				char answer[MAX_COUNTRY_SIZE];
				strcpy(answer,ptarray_of_countries + (long unsigned int) (n - 1) * MAX_COUNTRY_SIZE);
				bool z = (strcmp(answer, country_flag[ptarray_of_idxcountries[0]].country) == 0);
				_print_Result(z ? true : false, country_flag[ptarray_of_idxcountries[0]].country, answer);
				if (z) number_of_correct_answers++;
			}
			//---- Clear displayed flag
			if (FLAG_SUCCESS != flag_destroy()) {
				_error("Problème interne avec suppression affichage");
				goto EXIT;
			}
		}
		_print_Score( number_of_correct_answers, number_of_questions );
		again = _read_again();
	} while (again);
	//---- Exit ----------------------------------------------------------------
END:
	if (ptarray_of_idxcountries != NULL) free(ptarray_of_idxcountries);
	if (ptarray_of_countries != NULL) free(ptarray_of_countries);
	return EXIT_SUCCESS;
EXIT:
	if (ptarray_of_countries != NULL) free(ptarray_of_countries);
	if (ptarray_of_idxcountries != NULL) free(ptarray_of_idxcountries);
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------
