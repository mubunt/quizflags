//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel Rizzo.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: quizFlags
// QUIZ: All Country Flags of the World
//------------------------------------------------------------------------------
#ifndef DISPLAYFLAGS_H
#define DISPLAYFLAGS_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/Xlib.h>
#include <png.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define FLAG_SUCCESS				0
#define FLAG_FAILURE				-1
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct s_flag_description {
	Display 		*display;
	int 			screen;
	XImage 			*ximage;
	Window 			window;
	GC 				gc;
	int				x;
	int				y;
	unsigned int 	width;
	unsigned int 	height;
	unsigned int	steps;
	GC				progressgc;
	int 			yBar;
	int 			yLabel;
	unsigned char 	*data;
} s_flag_description;

extern int flag_create( char * );
extern int flag_destroy( void );
//------------------------------------------------------------------------------
#endif	// DISPLAYFLAGS_H
