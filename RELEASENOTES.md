# RELEASE NOTES: *quizFlags*, QUIZ: All Country Flags of the World.

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 1.0.1**:
  - Removed parasite source file.

**Version 1.0.0**:
  - First version.
