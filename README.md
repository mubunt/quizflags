 # *quizFlags*, QUIZ: All Country Flags of the World.

**quizFlags** est un quiz portant sur les drapeaux de tous les pays du monde. Pour chaque drapeau affiché, nommez le pays!
![Example 1](./README_images/quizFlags-01.png  "Example 1")

Lors de l'activation, le nombre de questions ansi que le nombre de propositions à émettre pour chaque question sont demandés.

Le nombre de questions doit être un entier dont la valeur est supérieure ou égale à 0 et inférieure ou égale à 10. Si la réponse est 0, le quiz se termine.

Le nombre de propositions doit être un entier dont la valeur est 0 ou bien supérieure ou égale à 2 et inférieure ou égale à 10. Si la réponse est 0, alors le joueur devra taper le nom du pays (majuscules, lettres accentuées et caractères comme le "-" peuvent être omis).

En fin de quiz, le score du joueur est affiché.

## LICENCE
**quizFlags** est couvert par la licence publique générale GNU (GPL) version 3 et supérieure.

## USAGE
``` bash
$ quizFlags -V
quizFlags - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
quizFlags - Version 1.0.0
$ quizFlags -h
quizFlags - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
quizFlags - Version 1.0.0

QUIZ: All Country Flags of the World

Usage: quizFlags [OPTIONS]

  -h, --help            Print this help and exit
  -V, --version         Print version and exit
$ quizFlags

```
## STRUCTURE DE L'APPLICATION
Cette section vous guide à travers la structure de ** quizFlags **. Une fois que vous aurez compris cette structure, vous trouverez facilement votre chemin dans la base de code de **quizFlags**.
``` bash
$ yaTree
./                                                      # Application level
├── README_images/                                      # Images for documentation
│   └── quizFlags-01.png                                # 
├── flags/                                              # Flag image directory
│   ├── Makefile                                        # Makefile
│   ├── afghanistan.png                                 # 
│   ├── afrique_du_sud.png                              # 
│   ├── ...                                             # 
│   └── zimbabwe.png                                    # 
├── icons/                                              # Icon directory
│   ├── Makefile                                        # Makefile
│   └── quizFlags.png                                   # Icon file for application shortcut
├── src/                                                # Source directory
│   ├── Makefile                                        # Makefile
│   ├── displayFlags.c                                  # Routines to display flags
│   ├── displayFlags.h                                  # Header of routines to display flags
│   ├── quizFlags.c                                     # Main source file
│   ├── quizFlags.c.orig                                # 
│   └── quizFlags.h                                     # Countries/Flags definition
├── COPYING.md                                          # GNU General Public License markdown file
├── LICENSE.md                                          # License markdown file
├── Makefile                                            # Makefile
├── README.md                                           # ReadMe markdown file
├── RELEASENOTES.md                                     # Release Notes markdown file
└── VERSION                                             # Version identification text file

4 directories, 249 files
$ 
```
## CONSTRUCTION DE L'APPLICATION
```Shell
$ cd quizFlags
$ make clean all
```

## INSTALLATION ET USAGE DE L'APPLICATION
```Shell
$ cd quizFlags
$ make release
    # Executable généré avec l'option d'optimisation -O2, est installé dans le répertoire $BIN_DIR (défini dans l'environnement de développement). Nous considérons que $BIN_DIR fait partie de PATH.
```

## NOTES
- Application et documentation en **français** car les noms des pays ont été enregistrés dans cette langue.

## LOGICIELS REQUIS
- Pour usage, rien de particulier...
- Pour son développement:
   - Bibliothèque *libsodium* installée (/usr/locallib et /usr/local/include), version 1.0.18. Se référer à https://github.com/jedisct1/libsodium.
- Developpé et testé avec XUBUNTU 20.04, GCC v9.3.0

## NOTES DE VERSION
Se référer au fichier [RELEASENOTES](./RELEASENOTES.md).

***